# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.13.1)

find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(HostSatMC)

set (
    CMAKE_C_FLAGS
    "${CMAKE_C_FLAGS} \
    -fshort-wchar \
"
)

add_definitions (-D__LITTLE_ENDIAN_BITFIELD)
target_include_directories (app PRIVATE
                            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)
target_sources(app PRIVATE
    src/main.c
    src/mctp_ctrl_msg_handler.c
    src/pldm_platform_handler.c)
