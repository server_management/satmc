# zephyr_HostSatMC
PoC of Host Side Band Satellite Management Controller

## Supported PLDM commands
|Requester/Responder|Message type|Command|
|-------------------|------------|-------|
|Responder| PLDM_BASE| PLDM_GET_TID|
|Responder| PLDM_BASE| PLDM_SET_TID|
|Responder| PLDM_BASE| PLDM_GET_PLDM_VERSION|
|Responder| PLDM_PLATFORM| PLDM_GET_TID|
|Responder| PLDM_PLATFORM| PLDM_SET_TID|
|Responder| PLDM_PLATFORM| PLDM_GET_PDR|
|Responder| PLDM_PLATFORM| PLDM_GET_STATE_SENSOR_READINGS|
|Responder| PLDM_PLATFORM| PLDM_GET_SENSOR_READING|
|Responder| PLDM_PLATFORM| PLDM_SET_EVENT_RECEIVER|
|Responder| PLDM_PLATFORM| PLDM_GET_EVENT_RECEIVER|
|Responder| PLDM_PLATFORM| PLDM_POLL_FOR_PLATFORM_EVENT_MESSAGE|
|Responder| PLDM_PLATFORM| PLDM_EVENT_MESSAGE_BUFFER_SIZE|
|Responder| PLDM_FRU| PLDM_GET_FRU_RECORD_TABLE|
|Requester| PLDM_PLATFORM| PLDM_PLATFORM_EVENT_MESSAGE|


## Support MCTP commands
|Requester/Responder|Command|
|-------------------|-------|
|Responder|MCTP_CTRL_CMD_SET_ENDPOINT_ID|
|Responder|MCTP_CTRL_CMD_GET_ENDPOINT_ID|