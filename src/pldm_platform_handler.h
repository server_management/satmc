/*
 * Copyright (c) 2021 ARM Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _PLDM_PLATFORM_HANDLER_H
#define _PLDM_PLATFORM_HANDLER_H

#include <zephyr.h>
#include <base.h>
#include <libmctp.h>
#include <platform.h>

#define MCTP_MSG_MAX_LEN 2048
#define STATIC_EID 0x12
#define MCTP_CTLR_MSG_TYPE 0x00
#define PLDM_MSG_TYPE 0x01

#define EVENT_MAX_SIZE 256
#define EVENT_QUEUE_SIZE 5
#define EVENT_MAX_BUFFER_SIZE 32

struct event
{
    uint32_t size;
    uint16_t id;
    uint8_t class;
    uint32_t checksum;
	uint8_t data[EVENT_MAX_SIZE];
};
struct pldm_event_queue
{
    int head;
    int tail;
    int length;
    struct k_mutex mutex;
    struct event events[EVENT_QUEUE_SIZE];
};

struct pldm
{
    struct mctp *mctp;
    uint8_t tid;
    uint8_t event_receiver_address_info;
    uint8_t event_message_global_enable;
    uint16_t event_terminus_max_buffer_size;
    uint16_t event_receiver_max_buffer_size;
    struct pldm_event_queue event_queue;
};

struct pldm *pldm_init(struct mctp *mctp);
int pldm_platform_handler(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len);
int gen_numeric_sensor_event(uint16_t sensor_id, uint8_t sensor_data_size, uint8_t present_reading, uint8_t present_state, uint8_t previous_state);
typedef int (*get_sensor_reading_imp)(struct pldm_get_sensor_reading_req *req_data, struct pldm_get_sensor_reading_resp *resp_data, size_t resp_data_buf_len);
typedef int (*get_state_sensor_readings_imp)(struct pldm_get_state_sensor_readings_req *req_data, struct pldm_get_state_sensor_readings_resp *resp_data, size_t resp_data_buf_len);

int event_queue_put(uint8_t *event_data, uint32_t event_size, uint8_t event_class, uint32_t checksum);
int event_queue_get(uint8_t *event_data, uint32_t *event_size, uint16_t *event_id, uint8_t *event_class, uint32_t *checksum);

struct pldm_sensor_event_numeric_sensor_states
{
    uint8_t present_event_state;
    uint8_t previous_event_state;
    uint8_t sensor_data_size;
    uint8_t sensor_event_message_enable;
    union
    {
        uint8_t uint8;
        int8_t sint8;
        uint16_t uint16;
        int16_t sint16;
        uint32_t uint32;
        int32_t sint32;
    } present_reading;
} __attribute__((packed));

struct pldm_sensor_event_state_sensor_states {
    uint8_t composite_sensor_count;
	uint8_t event_state[8];
	uint8_t previous_event_state[8];
} __attribute__((packed));

struct pldm_sensor_tbl;
typedef int (*update_sensor_state_imp)(struct pldm_sensor_tbl *);
struct pldm_sensor_tbl
{
    uint8_t sensor_id;
    struct pldm_sensor_event_sensor_op_state op_state;
    union
    {
        struct pldm_sensor_event_numeric_sensor_states numeric_sensor_states;
        struct pldm_sensor_event_state_sensor_states state_sensor_states;

    } state;
    update_sensor_state_imp update_sensor_state;
};

struct pldm_pdr_tbl
{
    uint32_t record_handle;
    uint32_t pdr_size;
    uint8_t *pdr_data;
};

#endif /* _MCTP_CTRL_MSG_HANDLER_H */