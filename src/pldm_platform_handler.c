/*
 * Copyright (c) 2021 ARM Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <sys/printk.h>
#include <assert.h>
#include <string.h>

#include <libmctp.h>
#include <libmctp-cmds.h>
#include <libmctp-i2c.h>
#include <platform.h>
#include <base.h>
#include <fru.h>

#include "pldm_platform_handler.h"

#define DEFAULT_TID 0x12
static struct pldm pldm;

extern struct pldm_sensor_tbl pldm_sensor_tbl[];
extern struct pldm_pdr_tbl pldm_pdr_tbl[];

const uint8_t fru_record_table[] = {
	0x0,  // record_set_id
	0x01, // record_type:general FRU record
	0x03, // number of FRU fields
	0x01, // encoding type=ASCII
		  // 5
	// FRU Field#1
	0x03, // type:part number
	0x06, // length
	'1',
	'2',
	'3',
	'4',
	'5',
	'6',
	// 9
	// FRU field#2
	0x04, // type:serial number
	0x07, // length
	'S',
	'N',
	'1',
	'2',
	'3',
	'4',
	'5',
	// 7
	// FRU field#3
	0x08, // type:name
	0x05, // length
	'S',
	'a',
	't',
	'M',
	'C',
};

static int pldm_get_pdr(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	struct pldm_get_pdr_req *req_data = (struct pldm_get_pdr_req *)req_msg->payload;
	struct pldm_get_pdr_resp *resp_data = (struct pldm_get_pdr_resp *)resp_msg->payload;

	uint8_t *dest = NULL;
	uint8_t *src = NULL;

	int ret = 0;

	resp_msg->hdr.request = PLDM_RESPONSE;
	resp_msg->hdr.datagram = 0;
	resp_msg->hdr.reserved = 0;
	resp_msg->hdr.instance_id = req_msg->hdr.instance_id;
	resp_msg->hdr.header_ver = PLDM_CURRENT_VERSION;
	resp_msg->hdr.type = req_msg->hdr.type;
	resp_msg->hdr.command = req_msg->hdr.command;
	ret += sizeof(struct pldm_msg_hdr);
	ret += sizeof(struct pldm_get_pdr_resp) - 1; // reduce record_data

	// find the record handle
	int i = 0;
	while (pldm_pdr_tbl[i].pdr_data != NULL)
	{
		if (pldm_pdr_tbl[i].record_handle == req_data->record_handle)
			break;
		i++;
	}

	if (pldm_pdr_tbl[i].pdr_data == NULL)
	{
		// record_handle is not in pldm_pdr_tbl
		resp_data->completion_code = PLDM_PLATFORM_INVALID_RECORD_HANDLE;
		resp_data->response_count = 0;
		return ret;
	}
	resp_data->next_record_handle = pldm_pdr_tbl[i + 1].record_handle;

	dest = resp_data->record_data;
	src = pldm_pdr_tbl[i].pdr_data;
	if (req_data->transfer_op_flag == 0x00)
		src += req_data->data_transfer_handle;

	resp_data->transfer_flag = 0x00;
	if ((req_data->data_transfer_handle + req_data->request_count) >= pldm_pdr_tbl[i].pdr_size)
	{
		resp_data->response_count = pldm_pdr_tbl[i].pdr_size - req_data->data_transfer_handle;
		resp_data->next_data_transfer_handle = 0x0; // there is no remaining data
		if (req_data->transfer_op_flag == 0x01)		// get first part
			resp_data->transfer_flag = 0x05;		// start and end
		else
			resp_data->transfer_flag = 0x04; // end
	}
	else
	{
		resp_data->response_count = req_data->request_count;
		resp_data->transfer_flag = 0x01; // middle
		resp_data->next_data_transfer_handle = req_data->data_transfer_handle + resp_data->response_count;
	}

	resp_data->completion_code = PLDM_SUCCESS;

	memcpy(dest, src, resp_data->response_count);
	ret += resp_data->response_count;
	struct pldm_pdr_hdr *pdr_hdr = (struct pldm_pdr_hdr *)dest;
	pdr_hdr->record_handle = pldm_pdr_tbl[i].record_handle;

	if (resp_data->transfer_flag == 0x04)
	{
		*(resp_buf + ret) = crc8(resp_data->record_data, resp_data->response_count);
		ret++;
	}

	return ret;
}

static int pldm_get_sensor_reading(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	struct pldm_get_sensor_reading_req *req_data = (struct pldm_get_sensor_reading_req *)req_msg->payload;
	struct pldm_get_sensor_reading_resp *resp_data = (struct pldm_get_sensor_reading_resp *)resp_msg->payload;
	int ret = 0;

	resp_msg->hdr.request = PLDM_RESPONSE;
	resp_msg->hdr.datagram = 0;
	resp_msg->hdr.reserved = 0;
	resp_msg->hdr.instance_id = req_msg->hdr.instance_id;
	resp_msg->hdr.header_ver = PLDM_CURRENT_VERSION;
	resp_msg->hdr.type = req_msg->hdr.type;
	resp_msg->hdr.command = req_msg->hdr.command;
	ret += sizeof(struct pldm_msg_hdr);

	struct pldm_sensor_tbl *sensor = NULL;
	for (int i = 0; pldm_sensor_tbl[i].sensor_id != 0x0; i++)
	{
		if (pldm_sensor_tbl[i].sensor_id == req_data->sensor_id)
		{
			sensor = &pldm_sensor_tbl[i];
		}
	}

	if (sensor)
	{
		resp_data->completion_code = PLDM_SUCCESS;
		resp_data->sensor_data_size = sensor->state.numeric_sensor_states.sensor_data_size;
		resp_data->sensor_operational_state = sensor->op_state.present_op_state;
		resp_data->sensor_event_message_enable = sensor->state.numeric_sensor_states.sensor_event_message_enable;
		resp_data->present_state = sensor->state.numeric_sensor_states.present_event_state;
		resp_data->previous_state = sensor->state.numeric_sensor_states.previous_event_state;
		resp_data->event_state = sensor->state.numeric_sensor_states.present_event_state;
		ret += sizeof(struct pldm_get_sensor_reading_resp);

		switch (sensor->state.numeric_sensor_states.sensor_data_size)
		{
		default:
		case PLDM_SENSOR_DATA_SIZE_SINT8:
		{
			int8_t *present_reading = (int8_t *)resp_data->present_reading;
			*present_reading = sensor->state.numeric_sensor_states.present_reading.sint8;
		}
		break;

		case PLDM_SENSOR_DATA_SIZE_UINT8:
		{
			uint8_t *present_reading = (uint8_t *)resp_data->present_reading;
			*present_reading = sensor->state.numeric_sensor_states.present_reading.uint8;
		}
		break;

		case PLDM_SENSOR_DATA_SIZE_SINT16:
		{
			int16_t *present_reading = (int16_t *)resp_data->present_reading;
			*present_reading = sensor->state.numeric_sensor_states.present_reading.sint16;
			ret += 1;
		}
		break;

		case PLDM_SENSOR_DATA_SIZE_UINT16:
		{
			uint16_t *present_reading = (uint16_t *)resp_data->present_reading;
			*present_reading = sensor->state.numeric_sensor_states.present_reading.uint16;
			ret += 1;
		}
		break;

		case PLDM_SENSOR_DATA_SIZE_SINT32:
		{
			int32_t *present_reading = (int32_t *)resp_data->present_reading;
			*present_reading = sensor->state.numeric_sensor_states.present_reading.sint32;
			ret += 3;
		}
		break;

		case PLDM_SENSOR_DATA_SIZE_UINT32:
		{
			uint32_t *present_reading = (uint32_t *)resp_data->present_reading;
			*present_reading = sensor->state.numeric_sensor_states.present_reading.uint32;
			ret += 3;
		}
		break;
		}
	}
	else
	{
		resp_data->completion_code = PLDM_PLATFORM_INVALID_SENSOR_ID;
		resp_data->sensor_data_size = PLDM_SENSOR_DATA_SIZE_SINT8;
		ret += sizeof(struct pldm_get_sensor_reading_resp);
	}

	return ret;
}

static int pldm_get_state_sensor_readings(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	struct pldm_get_state_sensor_readings_req *req_data = (struct pldm_get_state_sensor_readings_req *)req_msg->payload;
	struct pldm_get_state_sensor_readings_resp *resp_data = (struct pldm_get_state_sensor_readings_resp *)resp_msg->payload;
	size_t resp_data_buf_len = resp_buf_len - sizeof(struct pldm_msg_hdr);
	int ret = 0;

	resp_msg->hdr.request = PLDM_RESPONSE;
	resp_msg->hdr.datagram = 0;
	resp_msg->hdr.reserved = 0;
	resp_msg->hdr.instance_id = req_msg->hdr.instance_id;
	resp_msg->hdr.header_ver = PLDM_CURRENT_VERSION;
	resp_msg->hdr.type = req_msg->hdr.type;
	resp_msg->hdr.command = req_msg->hdr.command;
	ret += sizeof(struct pldm_msg_hdr);

	struct pldm_sensor_tbl *sensor = NULL;
	for (int i = 0; pldm_sensor_tbl[i].sensor_id != 0x0; i++)
	{
		if (pldm_sensor_tbl[i].sensor_id == req_data->sensor_id)
		{
			sensor = &pldm_sensor_tbl[i];
		}
	}

	if (sensor)
	{
		resp_data->completion_code = PLDM_SUCCESS;
		resp_data->field[0].sensor_op_state = sensor->op_state.present_op_state;
		resp_data->field[0].present_state = sensor->state.state_sensor_states.event_state[0];
		resp_data->field[0].previous_state = sensor->state.state_sensor_states.previous_event_state[0];
		resp_data->field[0].event_state = sensor->state.state_sensor_states.event_state[0];
		ret += sizeof(struct pldm_get_state_sensor_readings_resp);

		for (int i = 1; i < sensor->state.state_sensor_states.composite_sensor_count; i++)
		{
			resp_data->field[i].sensor_op_state = sensor->op_state.present_op_state;
			resp_data->field[i].present_state = sensor->state.state_sensor_states.event_state[i];
			resp_data->field[i].previous_state = sensor->state.state_sensor_states.previous_event_state[i];
			resp_data->field[i].event_state = sensor->state.state_sensor_states.event_state[i];
			ret += sizeof(get_sensor_state_field);
		}
	}
	else
	{
		resp_data->completion_code = PLDM_PLATFORM_INVALID_SENSOR_ID;
		resp_data->comp_sensor_count = 1;
		resp_data->field[0].sensor_op_state = PLDM_SENSOR_UNAVAILABLE;
		resp_data->field[0].present_state = PLDM_SENSOR_UNKNOWN;
		resp_data->field[0].previous_state = PLDM_SENSOR_UNKNOWN;
		resp_data->field[0].event_state = PLDM_SENSOR_UNKNOWN;
		ret += sizeof(struct pldm_get_state_sensor_readings_resp);
	}

	return ret;
}

static int pldm_get_tid(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	int ret;

	ret = encode_get_tid_resp(req_msg->hdr.instance_id, PLDM_SUCCESS, pldm.tid, resp_msg);
	if (ret)
		return -1;

	return sizeof(struct pldm_msg_hdr) + sizeof(struct pldm_get_tid_resp);
}

static int pldm_set_tid(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	int ret;

	pldm.tid = req_msg->payload[0];
	ret = encode_cc_only_resp(req_msg->hdr.instance_id, PLDM_BASE, /*PLDM_SET_TID*/ 0x01, PLDM_SUCCESS, resp_msg);
	if (ret)
		return -1;

	return sizeof(struct pldm_msg_hdr) + sizeof(uint8_t);
}

static int pldm_get_pldm_version(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	struct pldm_get_version_req *req_data = (struct pldm_get_version_req *)req_msg->payload;
	int ret = 0;
	struct ver_and_crc
	{
		ver32_t ver;
		uint32_t crc;
	};

	if (req_data->type == 0x0 && req_data->transfer_opflag == 0x01)
	{
		struct ver_and_crc base_ver = {
			{.major = 0xF1,
			 .minor = 0xF0,
			 .update = 0xF0,
			 .alpha = 0x0},
			0xBEE63CB3};
		encode_get_version_resp(req_msg->hdr.instance_id, PLDM_SUCCESS,
								0x0, 0x05, (const ver32_t *)&base_ver,
								sizeof(base_ver), resp_msg);
		ret = sizeof(struct pldm_msg_hdr) + 6 + sizeof(base_ver);
	}
	else
	{
		encode_get_version_resp(req_msg->hdr.instance_id, 0x83,
								0x0, 0x05, 0x0, 0x0, resp_msg);
		ret = sizeof(struct pldm_msg_hdr) + 6;
	}

	return ret;
}

static int pldm_get_pldm_types(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	uint64_t supportedTypes = (1 << PLDM_BASE) | (1 << PLDM_PLATFORM);

	encode_get_types_resp(req_msg->hdr.instance_id, PLDM_SUCCESS,
						  (uint8_t *)&supportedTypes, resp_msg);

	return sizeof(struct pldm_msg_hdr) + 9; //1 byte cc + 8 bytes types
}

static int pldm_get_fru_record_table(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	struct pldm_get_fru_record_table_resp *resp_data = (struct pldm_get_fru_record_table_resp *)resp_msg->payload;
	uint8_t *fru_record_table_data = resp_data->fru_record_table_data;
	int pad = 4 - (sizeof(fru_record_table) % 4); // pad
	int ret = 0;

	// dummy data
	resp_msg->hdr.request = PLDM_RESPONSE;
	resp_msg->hdr.datagram = 0;
	resp_msg->hdr.reserved = 0;
	resp_msg->hdr.instance_id = req_msg->hdr.instance_id;
	resp_msg->hdr.header_ver = PLDM_CURRENT_VERSION;
	resp_msg->hdr.type = req_msg->hdr.type;
	resp_msg->hdr.command = req_msg->hdr.command;
	ret += sizeof(struct pldm_msg_hdr);

	resp_data->completion_code = PLDM_SUCCESS;
	resp_data->next_data_transfer_handle = 0x0;
	resp_data->transfer_flag = 0x05;
	ret += sizeof(struct pldm_get_fru_record_table_resp) - 1;

	// todo: check if resp_buf_len is enough to hold resp data
	memcpy(fru_record_table_data, fru_record_table, sizeof(fru_record_table));
	ret += sizeof(fru_record_table);
	fru_record_table_data += sizeof(fru_record_table);

	// todo: clean pad bytes
	printk("pad=%d\n", pad);
	memset(fru_record_table_data, 0x0, pad);
	ret += pad;
	fru_record_table_data += pad;

	// todo: cal actual crc data
	*(fru_record_table_data++) = 0x01;
	*(fru_record_table_data++) = 0x02;
	*(fru_record_table_data++) = 0x03;
	*(fru_record_table_data++) = 0x04;
	ret += 4; // crc

	return ret;
}

static int pldm_set_event_receiver(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	int ret;
	uint8_t transport_protocol_type;
	uint8_t event_receiver_address_info;
	uint16_t heartbeat_timer;

	ret = decode_set_event_receiver_req(req_msg, req_len - sizeof(req_msg->hdr),
										&pldm.event_message_global_enable,
										&transport_protocol_type,
										&event_receiver_address_info,
										&heartbeat_timer);

	if (transport_protocol_type == 0x0)
	{
		pldm.event_receiver_address_info = event_receiver_address_info;
	}
	else
	{
		ret = PLDM_PLATFORM_INVALID_PROTOCOL_TYPE;
	}

	encode_set_event_receiver_resp(req_msg->hdr.instance_id, ret, resp_msg);
	return sizeof(struct pldm_msg_hdr) + PLDM_SET_EVENT_RECEIVER_RESP_BYTES;
}

static int pldm_get_event_receiver(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	struct pldm_get_event_receiver_resp *resp_data = (struct pldm_get_event_receiver_resp *)resp_msg->payload;
	int ret = 0;

	resp_msg->hdr.request = PLDM_RESPONSE;
	resp_msg->hdr.datagram = 0;
	resp_msg->hdr.reserved = 0;
	resp_msg->hdr.instance_id = req_msg->hdr.instance_id;
	resp_msg->hdr.header_ver = PLDM_CURRENT_VERSION;
	resp_msg->hdr.type = req_msg->hdr.type;
	resp_msg->hdr.command = req_msg->hdr.command;
	ret += sizeof(struct pldm_msg_hdr);

	resp_data->completion_code = PLDM_SUCCESS;
	resp_data->transport_protocol_type = 0x0; // MCTP
	resp_data->event_receiver_address = pldm.event_receiver_address_info;
	ret += sizeof(struct pldm_get_event_receiver_resp);

	return ret;
}

static int pldm_platform_event_message_resp_handler(uint8_t *msg, size_t msg_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *resp_msg = (struct pldm_msg *)msg;
	int ret;
	size_t payload_length;
	uint8_t completion_code;
	uint8_t platform_event_status;
	ret = decode_platform_event_message_resp(resp_msg,
											 payload_length,
											 &completion_code,
											 &platform_event_status);
	// react the response based on completion_code and platform_event_status
	printk("get response of platform event message command CC=0x%02x\n", completion_code);
	return 0;
}

int pldm_poll_for_platform_event_message(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	int ret;

	uint8_t format_version;
	uint8_t transfer_operation_flag;
	uint32_t data_transfer_handle;
	uint16_t event_id_to_acknowledge;

	ret = decode_poll_for_platform_event_message_req(req_msg, req_len - sizeof(req_msg->hdr),
													 &format_version, &transfer_operation_flag,
													 &data_transfer_handle, &event_id_to_acknowledge);
	if (ret != 0)
	{
		ret = encode_poll_for_platform_event_message_resp(req_msg->hdr.instance_id,
														  ret,
														  pldm.tid,
														  0x0,
														  0x0,
														  0x0,
														  0x0,
														  0x0,
														  NULL,
														  0x0,
														  resp_msg,
														  PLDM_POLL_FOR_PLATFORM_EVENT_MESSAGE_MIN_RESP_BYTES);
		return sizeof(struct pldm_msg_hdr) + PLDM_POLL_FOR_PLATFORM_EVENT_MESSAGE_MIN_RESP_BYTES;
	}

	static uint8_t event_data[EVENT_MAX_SIZE];
	static uint16_t event_id = 0;
	static uint32_t event_size = 0;
	static uint32_t checksum = 0;
	static uint8_t event_class = 0;

	uint8_t transfer_flag = PLDM_EVENT_START;
	uint8_t completion_code = PLDM_SUCCESS;
	uint16_t xfer_id;
	uint32_t max_xfer_size = (pldm.event_terminus_max_buffer_size > pldm.event_receiver_max_buffer_size) ? pldm.event_receiver_max_buffer_size : pldm.event_terminus_max_buffer_size;
	uint32_t xfer_size;
	uint32_t next_data_transfer_handle = 0;
	size_t payload_length = PLDM_POLL_FOR_PLATFORM_EVENT_MESSAGE_MIN_RESP_BYTES;

	switch (transfer_operation_flag)
	{
	case PLDM_EVENT_ACKNOWLEDGEMENT_ONLY:
		if (pldm.event_queue.length > 0)
		{
			xfer_id = 0xffff;
		}
		else
		{
			xfer_id = 0;
		}

		if (event_id == event_id_to_acknowledge)
		{
			event_id = 0; // mark event_data is empty
			completion_code = PLDM_SUCCESS;
		}
		else
		{
			completion_code = PLDM_PLATFORM_EVENT_ID_NOT_VALID;
		}

		data_transfer_handle = 0;
		next_data_transfer_handle = 0;
		xfer_size = 0;
		break;

	case PLDM_EVENT_GET_FIRST_PART:
		if (event_id == 0)
		{
			event_queue_get(event_data, &event_size, &event_id, &event_class, &checksum);
		}

		if (event_size >= max_xfer_size)
		{
			transfer_flag = PLDM_EVENT_START;
			xfer_size = max_xfer_size;
		}
		else
		{
			transfer_flag = PLDM_EVENT_START_AND_END;
			xfer_size = event_size;
		}

		xfer_id = event_id;
		data_transfer_handle = 0;
		next_data_transfer_handle = xfer_size;
		payload_length += xfer_size;
		break;

	case PLDM_EVENT_GET_NEXT_PART:
	default:
		transfer_flag = PLDM_EVENT_MIDDLE;
		xfer_id = event_id;
		if (event_id_to_acknowledge != 0xffff)
		{
			completion_code = PLDM_PLATFORM_EVENT_ID_NOT_VALID;
			xfer_size = 0;
			data_transfer_handle = 0;
			next_data_transfer_handle = 0;
			break;
		}

		if (event_id == 0x0 || data_transfer_handle > event_size)
		{
			completion_code = PLDM_PLATFORM_INVALID_DATA_TRANSFER_HANDLE;
			xfer_size = 0;
			data_transfer_handle = 0;
			next_data_transfer_handle = 0;
			break;
		}

		xfer_size = event_size - data_transfer_handle;
		if (xfer_size > max_xfer_size)
		{
			xfer_size = max_xfer_size;
		}
		next_data_transfer_handle = data_transfer_handle + xfer_size;
		payload_length += xfer_size;
		if ((xfer_size + sizeof(checksum)) <= max_xfer_size)
		{
			transfer_flag = PLDM_EVENT_END;
			payload_length += sizeof(checksum);
		}
		break;
	}

	ret = encode_poll_for_platform_event_message_resp(req_msg->hdr.instance_id, completion_code,
													  pldm.tid,
													  xfer_id,
													  next_data_transfer_handle,
													  transfer_flag,
													  event_class,
													  xfer_size,
													  &event_data[data_transfer_handle],
													  checksum,
													  resp_msg,
													  payload_length);
	printk("2 encode_poll_for_platform_event_message_resp ret=0x%02x\n", ret);
	return sizeof(struct pldm_msg_hdr) + payload_length;
}

static int pldm_event_message_max_buffer_size(uint8_t *req, size_t req_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req;
	struct pldm_event_message_buffer_size_req *req_data = (struct pldm_event_message_buffer_size_req *)req_msg->payload;
	struct pldm_msg *resp_msg = (struct pldm_msg *)resp_buf;
	struct pldm_event_message_buffer_size_resp *resp_data = (struct pldm_event_message_buffer_size_resp *)resp_msg->payload;
	int ret = 0;

	pldm.event_receiver_max_buffer_size = req_data->event_receiver_max_buffer_size;

	resp_msg->hdr.request = PLDM_RESPONSE;
	resp_msg->hdr.datagram = 0;
	resp_msg->hdr.reserved = 0;
	resp_msg->hdr.instance_id = req_msg->hdr.instance_id;
	resp_msg->hdr.header_ver = PLDM_CURRENT_VERSION;
	resp_msg->hdr.type = req_msg->hdr.type;
	resp_msg->hdr.command = req_msg->hdr.command;
	ret += sizeof(struct pldm_msg_hdr);

	resp_data->completion_code = PLDM_SUCCESS;
	resp_data->terminus_max_buffer_size = pldm.event_terminus_max_buffer_size;
	ret += sizeof(struct pldm_event_message_buffer_size_resp);

	return ret;
}

typedef int (*pldm_handler_fn)(uint8_t *msg, size_t msg_len, uint8_t *out_buf, size_t out_buf_len);
struct pldm_handler_tbl
{
	MessageType msg_type;
	uint8_t pldm_type;
	uint8_t command;
	pldm_handler_fn handler;
};

static struct pldm_handler_tbl pldm_handler_tbl[] = {
	{PLDM_REQUEST, PLDM_BASE, PLDM_GET_TID, pldm_get_tid},
	{PLDM_REQUEST, PLDM_BASE, /* PLDM_SET_TID */ 0x01, pldm_set_tid},
	{PLDM_REQUEST, PLDM_BASE, PLDM_GET_PLDM_VERSION, pldm_get_pldm_version},
	{PLDM_REQUEST, PLDM_BASE, PLDM_GET_PLDM_TYPES, pldm_get_pldm_types},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_GET_TID, pldm_get_tid},
	{PLDM_REQUEST, PLDM_PLATFORM, /* PLDM_SET_TID */ 0x01, pldm_set_tid},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_GET_PDR, pldm_get_pdr},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_GET_STATE_SENSOR_READINGS, pldm_get_state_sensor_readings},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_GET_SENSOR_READING, pldm_get_sensor_reading},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_SET_EVENT_RECEIVER, pldm_set_event_receiver},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_GET_EVENT_RECEIVER, pldm_get_event_receiver},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_POLL_FOR_PLATFORM_EVENT_MESSAGE, pldm_poll_for_platform_event_message},
	{PLDM_REQUEST, PLDM_PLATFORM, PLDM_EVENT_MESSAGE_BUFFER_SIZE, pldm_event_message_max_buffer_size},
	{PLDM_REQUEST, PLDM_FRU, PLDM_GET_FRU_RECORD_TABLE, pldm_get_fru_record_table},
	{PLDM_RESPONSE, PLDM_PLATFORM, PLDM_PLATFORM_EVENT_MESSAGE, pldm_platform_event_message_resp_handler},
	{0x0, 0x0, 0x0, NULL}};

int pldm_platform_handler(uint8_t *msg, size_t msg_len, uint8_t *resp_buf, size_t resp_buf_len)
{
	struct pldm_msg *pldm_msg = (struct pldm_msg *)(msg + 1); // skip first byte(msg type) of msg which is not part of pldm_msg
	size_t pldm_msg_len = msg_len - 1;
	struct pldm_header_info hdrFields;
	int ret;

	if (PLDM_SUCCESS != unpack_pldm_header(&pldm_msg->hdr, &hdrFields))
	{
		return -1;
	}

	for (int i = 0; pldm_handler_tbl[i].handler != NULL; i++)
	{
		if ((hdrFields.msg_type == pldm_handler_tbl[i].msg_type) &&
			(hdrFields.pldm_type == pldm_handler_tbl[i].pldm_type) &&
			(hdrFields.command == pldm_handler_tbl[i].command) &&
			(pldm_handler_tbl[i].handler != NULL))
		{
			printk("call msg_type 0x%02x pldm_type 0x%02x cmd 0x%02x handler\n", pldm_handler_tbl[i].msg_type, pldm_handler_tbl[i].pldm_type, pldm_handler_tbl[i].command);
			ret = pldm_handler_tbl[i].handler(pldm_msg, pldm_msg_len, resp_buf + 1, resp_buf_len - 1);
			if (ret > 0)
			{
				resp_buf[0] = PLDM_MSG_TYPE;
				ret++;
			}
			return ret;
		}
	}
	return -1; // not support
}

int pldm_platform_event_message(uint8_t event_class, uint8_t *event_data, size_t event_len, uint8_t *req_buf, size_t req_buf_len)
{
	struct pldm_msg *req_msg = (struct pldm_msg *)req_buf + 1;
	int ret;

	ret = encode_platform_event_message_req(
		0 /*instanceId*/, 1 /*formatVersion*/, pldm.tid /*tId*/, event_class,
		event_data, event_len, req_msg,
		event_len + PLDM_PLATFORM_EVENT_MESSAGE_MIN_REQ_BYTES);
	if (ret < 0)
	{
		return ret;
	}

	size_t req_len = sizeof(struct pldm_msg_hdr) + PLDM_PLATFORM_EVENT_MESSAGE_MIN_REQ_BYTES + event_len;
	if (req_len > req_buf_len)
	{
		return -1; // insufficient request buffer
	}
	return ret;
}

struct pldm_sensor_event_numeric_sensor_state_uint8
{
	uint16_t sensor_id;
	uint8_t sensor_event_class;
	uint8_t event_state;
	uint8_t previous_event_state;
	uint8_t sensor_data_size;
	uint8_t present_reading;
} __attribute__((packed));

// todo: support other sensor_data_size
int gen_numeric_sensor_event(uint16_t sensor_id, uint8_t sensor_data_size, uint8_t present_reading, uint8_t present_state, uint8_t previous_state)
{
	if (pldm.event_receiver_address_info == 0x0 || pldm.event_message_global_enable == PLDM_EVENT_MESSAGE_GLOBAL_DISABLE)
	{
		return 0;
	}

	const size_t event_len = sizeof(struct pldm_sensor_event_numeric_sensor_state_uint8);
	uint8_t event_data[event_len];
	struct pldm_sensor_event_numeric_sensor_state_uint8 *sensor_event = (struct pldm_sensor_event_numeric_sensor_state_uint8 *)event_data;

	sensor_event->sensor_id = sensor_id;
	sensor_event->sensor_event_class = PLDM_NUMERIC_SENSOR_STATE;
	sensor_event->event_state = previous_state;
	sensor_event->previous_event_state = present_state;
	sensor_event->sensor_data_size = sensor_data_size;
	sensor_event->present_reading = present_reading;

	if (pldm.event_message_global_enable == PLDM_EVENT_MESSAGE_GLOBAL_ENABLE_ASYNC || pldm.event_message_global_enable == PLDM_EVENT_MESSAGE_GLOBAL_ENABLE_ASYNC_KEEP_ALIVE)
	{
		const size_t req_buf_len = MCTP_MSG_MAX_LEN;
		uint8_t req_buf[req_buf_len];
		size_t req_len = 0;
		struct pldm_msg *req_msg = (struct pldm_msg *)(req_buf + 1);
		int ret;
		req_buf[0] = PLDM_MSG_TYPE;
		req_len++;

		ret = encode_platform_event_message_req(
			0 /*instanceId*/, 1 /*formatVersion*/, pldm.tid, PLDM_SENSOR_EVENT,
			event_data, sizeof(struct pldm_sensor_event_numeric_sensor_state_uint8), req_msg,
			event_len + PLDM_PLATFORM_EVENT_MESSAGE_MIN_REQ_BYTES);
		if (ret < 0)
		{
			return ret;
		}

		req_len += sizeof(struct pldm_msg_hdr);
		req_len += sizeof(struct pldm_platform_event_message_req) - 1;
		req_len += sizeof(struct pldm_sensor_event_numeric_sensor_state_uint8);
		if (req_len > req_buf_len)
		{
			return -1; // reqest message is too big
		}
		printk("gen_numeric_sensor_event req_len=%d dest eid=0x%02x\n", req_len, pldm.event_receiver_address_info);
		ret = mctp_message_tx(pldm.mctp, pldm.event_receiver_address_info, req_buf, req_len);
		// todo: check event size, if too big, put to fifo and sent an event_class=pldmMessagePollEvent instead.
		return ret;
	}
	else if (pldm.event_message_global_enable == PLDM_EVENT_MESSAGE_GLOBAL_ENABLE_POLLING)
	{
		//
	}
	// event message disabled
	return 0;
}

int event_queue_put(uint8_t *event_data, uint32_t event_size, uint8_t event_class, uint32_t checksum)
{
	static uint16_t event_id = 1;
	struct pldm_event_queue *queue = &pldm.event_queue;

	if (event_size > EVENT_MAX_SIZE)
	{
		return -1; // over max event size
	}

	k_mutex_lock(&queue->mutex, K_FOREVER);
	if (queue->tail == queue->head && queue->length > 0)
	{
		// queue is full
		// DSP0248 13.4 PLDM event log clearing policies
		// Policy: FIFO, when queue full, discard oldest event
		queue->head = (queue->tail + 1) % EVENT_QUEUE_SIZE;
		;
		printk("event_queue_put 1\n");
	}
	else
	{
		printk("event_queue_put 2\n");
		queue->length++;
	}
	queue->events[queue->tail].class = event_class;
	queue->events[queue->tail].id = event_id;
	queue->events[queue->tail].size = event_size;
	queue->events[queue->tail].checksum = checksum;
	memcpy(queue->events[queue->tail].data, event_data, event_size);
	queue->tail = (queue->tail + 1) % EVENT_QUEUE_SIZE;

	event_id++;
	if (event_id = 0xffff)
	{
		event_id = 1;
	}
	k_mutex_unlock(&queue->mutex);
	return 0;
}

int event_queue_get(uint8_t *event_data, uint32_t *event_size, uint16_t *event_id, uint8_t *event_class, uint32_t *checksum)
{
	struct pldm_event_queue *queue = &pldm.event_queue;
	k_mutex_lock(&queue->mutex, K_FOREVER);

	if (queue->length == 0)
	{
		*event_id = 0;
		*event_size = 0;
		*event_class = 0;
		*checksum = 0;
		printk("event_queue_get 1, empty queue\n");
	}
	else
	{
		*event_id = queue->events[queue->head].id;
		*event_size = queue->events[queue->head].size;
		*event_class = queue->events[queue->head].class;
		*checksum = queue->events[queue->head].checksum;
		memcpy(event_data, queue->events[queue->head].data, queue->events[queue->head].size);
		queue->head = (queue->head + 1) % EVENT_QUEUE_SIZE;
		queue->length--;
		printk("event_queue_get id=0x%04x, size=%d, class=0x%02x checksum=%08x\n", *event_id, *event_size, *event_class, *checksum);
	}
	k_mutex_unlock(&queue->mutex);
	return 0;
}

struct pldm *pldm_init(struct mctp *mctp)
{
	pldm.mctp = mctp;
	pldm.tid = DEFAULT_TID;
	pldm.event_receiver_address_info = 0x0;
	pldm.event_message_global_enable = 0x0;
	pldm.event_queue.head = 0;
	pldm.event_queue.tail = 0;
	pldm.event_queue.length = 0;
	pldm.event_terminus_max_buffer_size = EVENT_MAX_BUFFER_SIZE;
	pldm.event_receiver_max_buffer_size = EVENT_MAX_BUFFER_SIZE;
	k_mutex_init(&pldm.event_queue.mutex);
	return &pldm;
}
