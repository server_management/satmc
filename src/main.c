/*
 * Copyright (c) 2021 ARM, ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <sys/printk.h>
#include <assert.h>

#include <libmctp.h>
#include <libmctp-cmds.h>
#include <libmctp-i2c.h>
#include <drivers/i2c/slave/mctp_i2c_endpoint.h>
#include <drivers/gpio.h>
#include <drivers/i2c.h>

#include <platform.h>
#include <base.h>
#include <fru.h>

#include <utils.h>
#include "mctp_ctrl_msg_handler.h"
#include "pldm_platform_handler.h"

#include "UefiBaseTypes.h"
#include "Cper.h"

/* size of stack area used by each thread */
#define SENSOR_STACK_SIZE 4096
#define RESPONDER_STACK_SIZE 4096

K_THREAD_STACK_DEFINE(responder_stack_area, RESPONDER_STACK_SIZE);
static struct k_thread responder_data;

K_THREAD_STACK_DEFINE(sensor_stack_area, SENSOR_STACK_SIZE);
static struct k_thread sensor_data;

/* scheduling priority used by each thread */
#define PRIORITY 7

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS 5000

static struct pldm *pldm;

struct _pldm_cper_event_data_firmware_error_record_reference
{
	uint8_t format_version;
	uint8_t format_type;
	uint16_t event_data_length;
	EFI_ERROR_SECTION_DESCRIPTOR section_descriptor;
	EFI_FIRMWARE_ERROR_DATA section_data;
} __attribute__((packed));

static struct _pldm_cper_event_data_firmware_error_record_reference cper_event_data1 = {
	.format_version = 0x01,
	.format_type = 0x01,
	.event_data_length = sizeof(EFI_ERROR_SECTION_DESCRIPTOR) + sizeof(EFI_FIRMWARE_ERROR_DATA),
	.section_descriptor = {
		.SectionOffset = 72,
		.SectionLength = sizeof(EFI_FIRMWARE_ERROR_DATA),
		.Revision = 0x0100,
		.SecValidMask = 0x0,
		.SectionFlags = 0x0,
		.SectionType = {0x81212A96, 0x09ED, 0x4996, {0xB8, 0x63, 0x3E, 0x83, 0xED, 0x7C, 0x83, 0xB1}},
		.Severity = 3,
	},
	.section_data = {
		.ErrorType = 2,
		.Revision = 2,
		.RecordIdGuid = {0x12345678, 0xabcd, 0xdcba, {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}},
	},
};

static struct _pldm_cper_event_data_format_type_0 {
	EFI_COMMON_ERROR_RECORD_HEADER header;
	EFI_ERROR_SECTION_DESCRIPTOR descriptor1;
};

struct _pldm_event_data_numeric_sensor_state_uint8
{
	uint16_t sensor_id;
	uint8_t sensor_event_class;
	uint8_t event_state;
	uint8_t previous_event_state;
	uint8_t sensor_data_size;
	uint8_t presentReading;
} __attribute__((packed));

static struct _pldm_event_data_numeric_sensor_state_uint8 numeric_sensor_event_data1 = {
	.sensor_id = 0x01,
	.sensor_event_class = PLDM_NUMERIC_SENSOR_STATE,
	.previous_event_state = PLDM_SENSOR_NORMAL,
	.sensor_data_size = PLDM_SENSOR_DATA_SIZE_UINT8,
	.presentReading = 0x12,
};
struct _pldm_event_data_state_sensor_state
{
	uint16_t sensor_id;
	uint8_t sensor_event_class;
	uint8_t sensor_offset;
	uint8_t event_state;
	uint8_t previous_event_state;
} __attribute__((packed));

static struct _pldm_event_data_state_sensor_state state_sensor_event_data1 = {
	.sensor_id = 0x02,
	.sensor_event_class = PLDM_STATE_SENSOR_STATE,
	.sensor_offset = 1,
	.event_state = 0,
	.previous_event_state = 1,
};

struct _pldm_state_sensor_pdr
{
	uint16_t terminus_handle;
	uint16_t sensor_id;
	uint16_t entity_type;
	uint16_t entity_instance;
	uint16_t container_id;
	uint8_t sensor_init;
	bool8_t sensor_auxiliary_names_pdr;
	uint8_t composite_sensor_count;
	uint8_t possible_states[4];
} __attribute__((packed));

struct _pldm_numeric_sensor_pdr
{
	uint16_t terminus_handle;
	uint16_t sensor_id;
	uint16_t entity_type;
	uint16_t entity_instance;
	uint16_t container_id;
	uint8_t sensor_init;
	bool8_t sensor_auxiliary_names_pdr;
	uint8_t base_unit;
	int8_t uint_modifier;
	uint8_t rate_unit;
	uint8_t base_oem_unit_handle;
	uint8_t aux_unit;
	uint8_t aux_unit_modifier;
	uint8_t aux_rate_unit;
	uint8_t rel;
	uint8_t aux_oem_uint_handle;
	bool8_t is_linear;
	uint8_t sensor_data_size;
	real32_t resolution;
	real32_t offset;
	uint16_t accuracy;
	uint8_t plus_tolerance;
	uint8_t minus_tolerance;
	uint8_t hysteresis; //
	uint8_t supported_thresholds;
	uint8_t threshold_and_hysteresis_volatility;
	real32_t state_transition_interval;
	real32_t update_interval;
	uint8_t max_readable; //
	uint8_t min_readable; //
	uint8_t range_field_format;
	uint8_t range_field_support;
	uint8_t nominal_value; //
	uint8_t normal_max;	   //
	uint8_t normal_min;	   //
	uint8_t warning_high;  //
	uint8_t warning_low;   //
	uint8_t critical_high; //
	uint8_t critical_low;  //
	uint8_t fatal_high;	   //
	uint8_t fatal_low;	   //
} __attribute__((packed));

struct pldm_numeric_sensor_pdr_sensor1
{
	struct pldm_pdr_hdr hdr;
	struct _pldm_numeric_sensor_pdr pdr;
} __attribute__((packed));

static const struct pldm_numeric_sensor_pdr_sensor1 pdr_sensor1 = {
	.hdr = {
		.record_handle = 0x01,
		.version = 0x01,
		.type = PLDM_NUMERIC_SENSOR_PDR,
		.record_change_num = 0x0,
		.length = sizeof(struct _pldm_numeric_sensor_pdr)},
	.pdr = {.sensor_id = 0x01,
			.entity_type = 0x07, // phyical, pldm terminus
			.entity_instance = 0x0,
			.container_id = 0x0,
			.sensor_init = 0x0, // noInit
			.sensor_auxiliary_names_pdr = false,
			.base_unit = 2, // Degress C
			.uint_modifier = 0,
			.rate_unit = 0,
			.aux_unit = 0,
			.is_linear = true,
			.sensor_data_size = PLDM_SENSOR_DATA_SIZE_UINT8,
			.resolution = 1, // m
			.offset = 0,	 // B
			.accuracy = 100, // +- 1.00%
			.plus_tolerance = 0,
			.minus_tolerance = 0,
			.hysteresis = 0, // not use hysteresis
			.supported_thresholds = 0x0,
			.threshold_and_hysteresis_volatility = 0x1f,
			.state_transition_interval = 5,
			.update_interval = 1,
			.max_readable = 255,
			.min_readable = 0,
			.range_field_format = PLDM_SENSOR_DATA_SIZE_UINT8,
			.range_field_support = 0x0,
			.nominal_value = 0,
			.normal_max = 0,
			.normal_min = 0,
			.warning_high = 100,
			.warning_low = 0,
			.critical_high = 0,
			.critical_low = 0,
			.fatal_high = 0,
			.fatal_low = 0}};

struct pldm_state_sensor_pdr_sensor2
{
	struct pldm_pdr_hdr hdr;
	struct _pldm_state_sensor_pdr pdr;
} __attribute__((packed));

static const struct pldm_state_sensor_pdr_sensor2 pdr_sensor2 = {
	.hdr = {
		.record_handle = 0x02,
		.version = 0x01,
		.type = PLDM_STATE_SENSOR_PDR,
		.record_change_num = 0x0,
		.length = sizeof(struct _pldm_state_sensor_pdr)},
	.pdr = {.sensor_id = 0x02,
			.entity_type = 0x07, // phyical, pldm terminus
			.entity_instance = 0x0,
			.container_id = 0x0,
			.sensor_init = 0x0, // noInit;
			.sensor_auxiliary_names_pdr = false,
			.composite_sensor_count = 0x01,
			.possible_states = {0x01, 0x00, 0x01, 0x0f}}};

struct _pldm_entity_auxiliary_names_pdr
{
	uint16_t entity_type;
	uint16_t entity_instance_number;
	uint16_t entity_container_id;
	uint8_t shared_name_count;
	uint8_t name_string_count;
	uint8_t name_language_tag[3];
#if __BYTE_ORDER == __ORDER_BIG_ENDIAN__
	wchar_t entity_aux_name[6];
#else
	char entity_aux_name[12];
#endif
} __attribute__((packed));

struct pldm_entity_auxiliary_names_pdr1
{
	struct pldm_pdr_hdr hdr;
	struct _pldm_entity_auxiliary_names_pdr pdr;
} __attribute__((packed));

static const struct pldm_entity_auxiliary_names_pdr1 pdr_entity_aux_name1 = {
	.hdr = {
		.record_handle = 0x02,
		.version = 0x01,
		.type = PLDM_ENTITY_AUXILIARY_NAMES_PDR,
		.record_change_num = 0x0,
		.length = sizeof(struct _pldm_entity_auxiliary_names_pdr)},
	.pdr = {
		.entity_type = 36, // Management Controller Firmware
		.entity_instance_number = 1,
		.entity_container_id = 100,
		.shared_name_count = 0,
		.name_string_count = 1,
		.name_language_tag = "en",
#if __BYTE_ORDER == __ORDER_BIG_ENDIAN__
#error
		.entity_aux_name = L"SatMC",
#else
		.entity_aux_name = {0x0, 'S', 0x0, 'a', 0x0, 't', 0x0, 'M', 0x0, 'C', 0x0, 0x0},
#endif
	}};
struct _pldm_sensor_auxiliary_names_pdr
{
	uint16_t terminus_handle;
	uint16_t sensor_id;
	uint8_t sensor_count;
	uint8_t name_string_count;
	uint8_t name_language_tag[3];
#if __BYTE_ORDER == __ORDER_BIG_ENDIAN__
	wchar_t sensor_aux_name[9];
#else
	char sensor_aux_name[18];
#endif
} __attribute__((packed));

struct pldm_sensor_auxiliary_names_pdr1
{
	struct pldm_pdr_hdr hdr;
	struct _pldm_sensor_auxiliary_names_pdr pdr;
} __attribute__((packed));

static const struct pldm_sensor_auxiliary_names_pdr1 pdr_sensor_aux_name1 = {
	.hdr = {
		.record_handle = 0x02,
		.version = 0x01,
		.type = PLDM_SENSOR_AUXILIARY_NAMES_PDR,
		.record_change_num = 0x0,
		.length = sizeof(struct _pldm_sensor_auxiliary_names_pdr)},
	.pdr = {
		.sensor_id = 1,
		.sensor_count = 1,
		.name_string_count = 1,
		.name_language_tag = "en",
#if __BYTE_ORDER == __ORDER_BIG_ENDIAN__
		.sensor_aux_name = L"CoreTemp",
#else
		.sensor_aux_name = {0x0, 'C', 0x0, 'o', 0x0, 'r', 0x0, 'e', 0x0, 'T', 0x0, 'e', 0x0, 'm', 0x0, 'p', 0x0, 0x0},
#endif
	}};

struct pldm_pdr_tbl pldm_pdr_tbl[] = {
	{0, sizeof(pdr_sensor1), (uint8_t *)&pdr_sensor1},
	{1, sizeof(pdr_sensor2), (uint8_t *)&pdr_sensor2},
	{2, sizeof(pdr_entity_aux_name1), (uint8_t *)&pdr_entity_aux_name1},
	{3, sizeof(pdr_sensor_aux_name1), (uint8_t *)&pdr_sensor_aux_name1},
	{0}};

static int update_sensor_state_sensor1(struct pldm_sensor_tbl *sensor)
{
	static uint8_t cnt = 0;
	static uint8_t reading = 0x10;

	sensor->op_state.previous_op_state = sensor->op_state.present_op_state;
	sensor->op_state.present_op_state = PLDM_SENSOR_ENABLED;

	sensor->state.numeric_sensor_states.previous_event_state = sensor->state.numeric_sensor_states.present_event_state;
	sensor->state.numeric_sensor_states.present_event_state = PLDM_SENSOR_NORMAL;
	sensor->state.numeric_sensor_states.sensor_data_size = PLDM_SENSOR_DATA_SIZE_UINT8;
	sensor->state.numeric_sensor_states.sensor_event_message_enable = PLDM_EVENTS_ENABLED;
	sensor->state.numeric_sensor_states.present_reading.uint8 = reading;
#if 0
	cnt++;
	if (cnt > 10)
	{
		cnt = 0;
		gen_numeric_sensor_event(sensor->sensor_id, sensor->state.numeric_sensor_states.sensor_data_size, sensor->state.numeric_sensor_states.present_reading.uint8, sensor->state.numeric_sensor_states.present_event_state, sensor->state.numeric_sensor_states.previous_event_state);
	}
#endif
	return PLDM_SUCCESS;
}

static int update_sensor_state_sensor2(struct pldm_sensor_tbl *sensor)
{
	sensor->op_state.previous_op_state = sensor->op_state.present_op_state;
	sensor->op_state.present_op_state = PLDM_SENSOR_ENABLED;

	sensor->state.state_sensor_states.previous_event_state[0] = sensor->state.state_sensor_states.event_state[0];
	sensor->state.state_sensor_states.event_state[0] = PLDM_SENSOR_NORMAL;

	return PLDM_SUCCESS;
}

struct pldm_sensor_tbl pldm_sensor_tbl[] = {
	{
		.sensor_id = 0x01,
		.update_sensor_state = update_sensor_state_sensor1,
	},
	{
		.sensor_id = 0x02,
		.update_sensor_state = update_sensor_state_sensor2,
	},
	{0}};

static void rx_message(uint8_t src_eid, void *data, void *msg, size_t len)
{
	struct mctp *mctp = (struct mctp *)data;
	uint8_t type = ((uint8_t *)msg)[0];
	uint8_t resp_buf[MCTP_MSG_MAX_LEN];
	int resp_len = 0;

	printk("rx mctp message type(%x) len=%d\n", type, len);
	switch (type)
	{
	case MCTP_CTLR_MSG_TYPE:
		resp_len = mctp_ctrl_msg_handler(msg, len, resp_buf, sizeof(resp_buf));
		break;
	case PLDM_MSG_TYPE:
		resp_len = pldm_platform_handler(msg, len, resp_buf, sizeof(resp_buf));
		break;
	default:
		break;
	}
	if (resp_len > 0)
	{
		mctp_message_tx(mctp, src_eid, resp_buf, resp_len);
	}
}

static int ep_tx_fn(const void *data, uint8_t *buf, uint32_t len)
{
	const struct device *dev = data;
	uint8_t *pkt = buf;
	printk("ep_tx_fn dest=0x%02x\n", pkt[0]);
	for (int i = 0; i < len; i++)
	{
		printk(" 0x%02x", pkt[i]);
	}
	printk("\n");
	mctp_i2c_endpoint_write(dev, pkt + 1, len - 1, pkt[0]);
	return 0;
}

void responderThread(void *dummy1, void *dummy2, void *dummy3)
{
	ARG_UNUSED(dummy3);
	const struct device *ep_dev;
	struct mctp_binding_i2c *mctp_i2c;
	struct mctp *mctp;

	uint8_t buf[256];
	int ret;

	ep_dev = device_get_binding("EP_0");
	if (!ep_dev)
	{
		return;
	}
	i2c_slave_driver_register(ep_dev);

	mctp = mctp_init();
	assert(mctp);

	pldm = pldm_init(mctp);
	assert(pldm);

	mctp_i2c = mctp_i2c_init(mctp_i2c_endpoint_addr(ep_dev));
	assert(mctp_i2c);

	mctp_i2c_set_tx_fn(mctp_i2c, ep_tx_fn, ep_dev);
	mctp_register_bus(mctp, mctp_binding_i2c_core(mctp_i2c), STATIC_EID);
	mctp_set_rx_all(mctp, rx_message, mctp);

	// test code, queue a event
	uint8_t event[] = "123456789";
	event_queue_put(&event, sizeof(event), 0xf0, 0x12345678);
	event_queue_put(&numeric_sensor_event_data1, sizeof(numeric_sensor_event_data1), PLDM_SENSOR_EVENT, 0x12345678);
	event_queue_put(&state_sensor_event_data1, sizeof(state_sensor_event_data1), PLDM_SENSOR_EVENT, 0x12345678);
	event_queue_put(&cper_event_data1, sizeof(cper_event_data1), PLDM_CPER_EVENT, 0x12345678);

	while (1)
	{
		ret = mctp_i2c_endpoint_read(ep_dev, buf, 256);

		printk("rec %d bytes,\n", ret);
		for (int i = 0; i < ret; i++)
		{
			printk(" 0x%02x", buf[i]);
		}
		printk("\n");
		ret = mctp_i2c_rx(mctp_i2c, buf, ret);
		printk("mctp_i2c_read() ret=%d\n", ret);
		// clean up the buf
	}
}

void sensorThread(void *dummy1, void *dummy2, void *dummy3)
{
	ARG_UNUSED(dummy1);
	ARG_UNUSED(dummy2);
	ARG_UNUSED(dummy3);

	while (1)
	{
		for (int i = 0; pldm_sensor_tbl[i].sensor_id != 0; i++)
		{
			pldm_sensor_tbl[i].update_sensor_state(&pldm_sensor_tbl[i]);
		}
		k_msleep(SLEEP_TIME_MS);
	}
}

void main(void)
{
	k_thread_create(&responder_data, responder_stack_area,
					K_THREAD_STACK_SIZEOF(responder_stack_area),
					responderThread, NULL, NULL, NULL,
					PRIORITY, 0, K_FOREVER);
	k_thread_name_set(&responder_data, "responder");
	k_thread_start(&responder_data);

	k_thread_create(&sensor_data, sensor_stack_area,
					K_THREAD_STACK_SIZEOF(sensor_stack_area),
					sensorThread, NULL, NULL, NULL,
					PRIORITY, 0, K_FOREVER);
	k_thread_name_set(&sensor_data, "sensor");
	k_thread_start(&sensor_data);
}
