/*
 * Copyright (c) 2021 ARM Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _MCTP_CTRL_MSG_HANDLER_H
#define _MCTP_CTRL_MSG_HANDLER_H

int mctp_ctrl_msg_handler(uint8_t *msg, size_t msg_len, uint8_t *rep_buf, size_t rep_buf_len);

#endif /* _MCTP_CTRL_MSG_HANDLER_H */