/*
 * Copyright (c) 2021 ARM Ltd
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <sys/printk.h>
#include <assert.h>
#include <libmctp.h>
#include <libmctp-cmds.h>
#include <libmctp-i2c.h>
#include "mctp_ctrl_msg_handler.h"

#define STATIC_EID 0x12

static int mctp_ctrl_cmd_set_endpoint_id(uint8_t *req, size_t req_len, uint8_t *rep_buf, size_t rep_buf_len)
{
	struct mctp_ctrl_msg_hdr *rep_hdr = (struct mctp_ctrl_msg_hdr*)rep_buf;
	struct mctp_ctrl_msg_hdr *req_hdr = (struct mctp_ctrl_msg_hdr*)req;
	uint8_t inst = req_hdr->rq_dgram_inst & MCTP_CTRL_HDR_INSTANCE_ID_MASK;
	int ret = sizeof(struct mctp_ctrl_msg_hdr);

	if (req_len < (sizeof(struct mctp_ctrl_msg_hdr)+2)) {
		return -1;
	}

	if (rep_buf_len < (ret + 3)) {
		return -2;
	}

	rep_hdr->ic_msg_type = MCTP_CTRL_HDR_MSG_TYPE;
	rep_hdr->rq_dgram_inst = inst;
	rep_hdr->command_code = req_hdr->command_code;
	rep_hdr->completion_code = MCTP_CTRL_CC_SUCCESS;

	rep_buf[ret++] = 0x10;	//[5:4]=01b, EID assignmeet rejected.
							//[1:0]=00b, Device does not use EID pool.
	rep_buf[ret++] = STATIC_EID;
	rep_buf[ret++] = 0x00; 	// EID pool size, 0x00=no dynamic EID pool

	return ret;
}

static int mctp_ctrl_cmd_get_endpoint_id(uint8_t *req, size_t req_len, uint8_t *rep_buf, size_t rep_buf_len)
{
	struct mctp_ctrl_msg_hdr *rep_hdr = (struct mctp_ctrl_msg_hdr*)rep_buf;
	struct mctp_ctrl_msg_hdr *req_hdr = (struct mctp_ctrl_msg_hdr*)req;
	uint8_t inst = req_hdr->rq_dgram_inst & MCTP_CTRL_HDR_INSTANCE_ID_MASK;
	int ret = sizeof(struct mctp_ctrl_msg_hdr);

    //check request data len
	if (req_len < sizeof(struct mctp_ctrl_msg_hdr))
		return -1;

    //check response buf size
	if (rep_buf_len < (ret + 3))
		return -2;

    rep_hdr->ic_msg_type = MCTP_CTRL_HDR_MSG_TYPE;
	rep_hdr->rq_dgram_inst = inst;
	rep_hdr->command_code = req_hdr->command_code;
	rep_hdr->completion_code = MCTP_CTRL_CC_SUCCESS;

    rep_buf[ret++] = STATIC_EID;
    rep_buf[ret++] = 0x02;  //[5:4]endpoint type: simple endpoint(00b)
                            //[1:0]endpoint id type: static EID supported
    rep_buf[ret++] = 0x00;  //medium-specific information

    return ret;
}

typedef int (*mctp_ctrl_msg_fn)(uint8_t *req, size_t req_len, uint8_t *rep_buf, size_t rep_buf_len);
struct mctp_ctrl_msg_fn {
	uint8_t  command_code;
	mctp_ctrl_msg_fn handler;
};

static struct mctp_ctrl_msg_fn mctp_ctrl_msg_fn_tbl[] = {
	{MCTP_CTRL_CMD_SET_ENDPOINT_ID, mctp_ctrl_cmd_set_endpoint_id},
    {MCTP_CTRL_CMD_GET_ENDPOINT_ID, mctp_ctrl_cmd_get_endpoint_id},
	{MCTP_CTRL_CMD_RESERVED, NULL}
};

int mctp_ctrl_msg_handler(uint8_t *msg, size_t msg_len, uint8_t *rep_buf, size_t rep_buf_len)
{
	struct mctp_ctrl_msg_hdr *msg_hdr = (struct mctp_ctrl_msg_hdr*)msg;

	for(int i=0;mctp_ctrl_msg_fn_tbl[i].handler != NULL; i++) {
		printk("req cmd 0x%02x matching tbl cmd 0x%02x\n",msg_hdr->command_code, mctp_ctrl_msg_fn_tbl[i].command_code);
		if ((mctp_ctrl_msg_fn_tbl[i].command_code == msg_hdr->command_code) && 
			(mctp_ctrl_msg_fn_tbl[i].handler != NULL)) {
			return mctp_ctrl_msg_fn_tbl[i].handler(msg, msg_len, rep_buf, rep_buf_len);
		}
	}
	return -1; //not support
}